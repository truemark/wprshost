#!/usr/bin/env bash

set -euo pipefail
cd "$(dirname "${0}")" || exit 1
git pull
git submodule update --init
