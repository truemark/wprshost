ufw:
  enabled: True
  services:
    ssh:
      protocol: tcp
      from_addr:
        - 0.0.0.0/0
    "2020":
      protocol: tcp
      from_addr:
        - 0.0.0.0/0
    http:
      protocol: tcp
      from_addr:
        - 0.0.0.0/0
    https:
      protocol: tcp
      from_addr:
        - 0.0.0.0/0
