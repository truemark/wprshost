# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define "wprshost" do |wprshost|

    wprshost.vm.box = "truemark/ubuntu-20.04-basic"

    wprshost.vm.hostname = "wprshost"

    # Add additional vmdk file
    wprshost.vm.provider "vmware_fusion" do |vm, override|
      vdiskmanager = '/Applications/VMware\ Fusion.app/Contents/Library/vmware-vdiskmanager'
      dir = Dir.pwd + "/.vagrant/additional-disk"
      unless File.directory?( dir )
        Dir.mkdir dir
      end
      file_to_disk = "#{dir}/wprshost-zfs.vmdk"
      unless File.exists?( file_to_disk )
        `#{vdiskmanager} -c -s 50GB -a lsilogic -t 1 #{file_to_disk}`
      end
      vm.vmx['scsi0:1.filename'] = file_to_disk
      vm.vmx['scsi0:1.present']  = 'TRUE'
      vm.vmx['scsi0:1.redo']     = ''
    end

    # Delete vmdk files after destruction
    config.trigger.after :destroy do |t|
      dir = Dir.pwd + "/.vagrant/additional-disk"
      t.run = {inline: "bash -c 'rm -rf #{dir}/wprshost*'"}
    end

  end

  # Show GUI instead of run headless
#   config.vm.provider "vmware_fusion" do |v, override|
#     v.gui = true
#   end

  # Configure CPU and Memory
  # https://www.vagrantup.com/docs/providers/configuration.html
  config.vm.provider "vmware_fusion" do |v, override|
    v.vmx["memsize"] = "4096"
    v.vmx["numvcpus"] = "2"
  end

  # Share folder
  config.vm.synced_folder ".", "/srv/salt"

  # Execute bootstrap
  config.vm.provision "shell", privileged: true, inline: <<-SHELL
    /srv/salt/bootstrap.sh
  SHELL
end
