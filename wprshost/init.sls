dependencies:
  pkg.installed:
    - pkgs:
      - ufw
      - screen
      - tmux
      - apache2
      - mysql-server
      - php-fpm
      - php-common
      - php-mbstring
      - php-xmlrpc
      - php-soap
      - php-gd
      - php-xml
      - php-intl
      - php-mysql
      - php-cli
      - php-ldap
      - php-zip
      - php-curl
      - php-getid3
      - php-ssh2
      - php-sqlite3
      - php-pear
      - certbot
      - python3-certbot-apache

disable_default_site:
  cmd.run:
    - name: a2dissite 000-default && systemctl reload apache2
    - onlyif:
      - a2query -s 000-default

enable_rewrite:
  cmd.run:
    - name: a2enmod rewrite
    - unless:
      - a2query -m rewrite

enable_proxy:
  cmd.run:
    - name: a2enmod proxy_fcgi
    - unless:
      - a2query -m proxy_fcgi

enable_ssl:
  cmd.run:
    - name: a2enmod ssl
    - unless:
      - a2query -m ssl

files:
  file.managed:
    - names:
      - /etc/apache2/sites-enabled/wordpress.conf:
        - source: salt://wprshost/files/httpd-wordpress.conf
      - /var/www/conf/server.crt:
        - source: salt://wprshost/files/server.crt
      - /var/www/conf/server.key:
        - source: salt://wprshost/files/server.key
    - user: root
    - group: root
    - mode: 644
    - makedirs: True

{% if not salt['file.file_exists' ]('/var/www/conf/include.conf') %}
include_file:
  file.managed:
    - names:
      - /var/www/conf/include.conf:
        - source: salt://wprshost/files/httpd-include.conf
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
{% endif %}

'systemctl reload apache2':
  cmd.run:
    - require:
      - files
      - enable_ssl
      - enable_rewrite
      - enable_proxy
      - disable_default_site
      - dependencies

wordpress_install:
  archive.extracted:
    - name: /var/www/html
    - source: https://wordpress.org/latest.tar.gz
    - options: "--strip-components=1"
    - skip_verify: True
    - enforce_toplevel: False
    - onlyif:
      - "[[ $(ls -1 /var/www/html | wc -l) == 1 ]] && [[ -f /var/www/html/index.html ]]"

remove_index:
  cmd.run:
    - name: rm -f /var/www/html/index.html
    - onlyif:
      - "[[ -f /var/www/html/index.html ]]"

/etc/cron.daily/certbot:
  file.managed:
    - source: salt://wprshost/files/certbot.sh
    - user: root
    - group: root
    - mode: 755

/usr/local/bin/wp:
  file.managed:
    - source: https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    - user: root
    - group: root
    - mode: 755
    - skip_verify: True
