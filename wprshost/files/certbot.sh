#!/usr/bin/env bash

certbot renew --post-hook "systemctl reload apache2"
