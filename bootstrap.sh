#!/usr/bin/env bash

###############################################################################
# TrueMark WordPress Host Bootstrap Script
#
# To bootstrap from the latest, run the following
# bash <(curl -sSL https://bitbucket.org/truemark/wprshost/raw/master/bootstrap.sh)
###############################################################################

set -euo pipefail

#------------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------------
[[ -n "${URL+z}" ]] || URL="https://bitbucket.org/truemark/wprshost.git"

#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------
function package_exists {
  set +e
  dpkg -s "${1}" &> /dev/null
  val=$?
  set -e
  return $val
}

#------------------------------------------------------------------------------
# Checks
#------------------------------------------------------------------------------
[[ -f /etc/lsb-release ]] || (echo "Missing /etc/lsb-release, exiting" && exit 1)
source /etc/lsb-release
[[ "${DISTRIB_RELEASE}" == '20.04' ]] || (echo "Ubuntu 20.04 required, exiting" && exit 1)

#------------------------------------------------------------------------------
# Corrections
#------------------------------------------------------------------------------
# This corrects an issue with TrueMark Ubuntu 20.04 Basic
[[ -e "/etc/issue.original" ]] && mv /etc/issue.orgiginal /etc/issue

#------------------------------------------------------------------------------
# Update & Install Dependencies
#------------------------------------------------------------------------------
echo "Checking dependencies..."
apt-get update -qq
if ! package_exists curl; then apt-get install -y curl; fi
if ! package_exists gnupg2; then apt-get install -y gnupg2; fi
if ! package_exists git; then apt-get install -y git; fi
if ! package_exists python3-pygit2; then apt-get install -y python3-pygit2; fi

#------------------------------------------------------------------------------
# Download and Unpack
#------------------------------------------------------------------------------
if [[ ! -d /srv/salt ]]; then
  git clone "${URL}" /srv/salt --recursive
else
  echo "/srv/salt exists, skipping creation"
fi

#------------------------------------------------------------------------------
# Install Salt Minion
#------------------------------------------------------------------------------
if [[ ! -f /etc/salt/minion.d/masterless.conf ]]; then
  mkdir -p /etc/salt/minion.d
  ln -s /srv/salt/masterless.conf /etc/salt/minion.d/masterless.conf
fi

if ! package_exists "salt-minion"; then
  curl -sSL "https://repo.saltstack.com/py3/ubuntu/${DISTRIB_RELEASE}/amd64/latest/SALTSTACK-GPG-KEY.pub" | apt-key add -
  echo "deb http://repo.saltstack.com/py3/ubuntu/${DISTRIB_RELEASE}/amd64/latest focal main" > /etc/apt/sources.list.d/saltstack.list
  apt-get update && apt-get install salt-minion -y
  systemctl stop salt-minion
  systemctl disable salt-minion
else
  echo "salt-minion already installed, skipping"
fi

echo "Bootstrap completed!"
